#!/usr/bin/env python3

import asyncio, logging
from asyncua import Server, ua
import sys, subprocess, os

import gpiod
from gpiod.line import Direction, Value

# lines:

#defaults and definitions
CLICKBOARD_HEADER = 0
LINE_MBUS_AN  = ["x",0,0,0,0]
LINE_MBUS_RST = ["x",0,0,0,0]
LINE_MBUS_EN  = ["x",0,0,0,0]

PWM_PIN       = ["x",0,0,0,0]
PWM_BEAGLE    = ["x",0,0,0,0]
req_mbus_an   = ["x",0,0,0,0]
req_mbus_rst  = ["x",0,0,0,0]
req_mbus_en   = ["x",0,0,0,0]


PORT_1 = "4841"
PORT_2 = "4842"
PORT_3 = "4843"
PORT_4 = "4844"

PWM_class = "/sys/class/pwm/"
chip_gpio2=gpiod.Chip("/dev/gpiochip2")   #P8, Pin3  .. Pin30
chip_gpio3=gpiod.Chip("/dev/gpiochip3")   #P9, Pin 12(1), 15(4), 23(10), 25(12), 27(14), 30(17), 41(19), value in bracket is bit in GPIO
chip_gpio4=gpiod.Chip("/dev/gpiochip4")   #P9, Pin31 .. Pin46
no_op=0

#P9: 10, 28

#P8, Pin 3..30 = GPIO 2
#Clickboard header 1:
LINE_MBUS_AN[1]  = 23  #P8_23 (GPIO2)
LINE_MBUS_RST[1] = 1   #P9_12 (GPIO3)
#LINE_MBUS_EN[1]  = 0   #P9_28 #placeholder, SPI_1_SS1
PWM_PIN[1]       = "pwmchip16/pwm0"
PWM_BEAGLE[1]    = PWM_class + PWM_PIN[1] #"pwmchip16"

#Clickboard header 2:
# directly accessible in basic cape design
LINE_MBUS_AN[2]  = 12 #P8_12  (GPIO2)
LINE_MBUS_RST[2] = 10 #P9_10, (GPIO3)
#LINE_MBUS_EN[2]  = 0  # placeholder, PWM ==> PWM_beagle_P9_42
PWM_PIN[2]        = "pwmchip16/pwm1"
PWM_BEAGLE[2]     = PWM_class + PWM_PIN[2]#"pwmchip16"

#Clickboard header 3:
LINE_MBUS_AN[3]  = 9  #P8_09  (GPIO2)
LINE_MBUS_RST[3] = 11 #P8_13  (GPIO2)
#LINE_MBUS_EN[3]  = 0  #placeholder, SPI_0_SS1
PWM_PIN[3]       = "pwmchip0/pwm0"
PWM_BEAGLE[3]    = PWM_class + PWM_PIN[3] #"pwmchip0"

#Clickboard header 4:
# directly accessible in basic cape design
LINE_MBUS_AN[4]  = 8  #P8_08  (GPIO2)
LINE_MBUS_RST[4] = 13 #P8_16  (GPIO2)
LINE_MBUS_EN[4]  = 7  #P8_07  (GPIO2)
PWM_PIN[4]       = "pwmchip0/pwm1"
PWM_BEAGLE[4]    = PWM_class + PWM_PIN[4] #"pwmchip0"
#pwmchip0/pwm1/period'

#PWM on Beagle-Board, used on header #2
EN_PWM_PIN= PWM_class + "pwmchip32/pwm0"   #P9_42
#print(EN_PWM_PIN)

async def reset_motor_mult(LINE_MBUS_RST, req_mbus_rst):
    # program the RST pin on Mikrobus connector to logic 0
    req_mbus_rst.set_value(LINE_MBUS_RST,Value.INACTIVE)

async def set_direction_mult(LINE_MBUS_AN, direction, req_mbus_an):
    if (direction==0):
       req_mbus_an.set_value(LINE_MBUS_AN,Value.INACTIVE)
    else:
       req_mbus_an.set_value(LINE_MBUS_AN,Value.ACTIVE)


async def set_pwm_duty_cycle_mult(pwm_pin, duty_cycle):
    with open(f"{pwm_pin}/duty_cycle", "w") as duty_cycle_file:
        duty_cycle_file.write(str(duty_cycle))

async def set_pwm_rate_mult(pwm_pin, rate):
    with open(f"{pwm_pin}/period", "w") as period_file:
        period_file.write(str(rate))


async def enable_motor_mult(CLICKBOARD_HEADER):
    # global req_mbus_en
    # Header 1: SPI_1_SS1
    # Header 2:  PWM
    # Header 3: SPI_0_SS1
    # Header 4: P8_07
    match CLICKBOARD_HEADER:
        case 1:
            # print("Enable for Clickboard-Header " + str(CLICKBOARD_HEADER) + " not implemented yet!")
            # The enable-signal on header 1 is set to 1 by default, no settings are required
            no_op
        case 2:
            # On Header2 the enable-signal is driven by a PWM, hence this workaround
            await set_pwm_duty_cycle_mult(EN_PWM_PIN, 1000)
            await set_pwm_rate_mult(EN_PWM_PIN, 1000)
            command_path= EN_PWM_PIN + "/enable"
            # Enable the channel
            subprocess.run("echo 1 > " + command_path, shell=True)
        case 3:
            #print("Enable for Clickboard-Header " + str(CLICKBOARD_HEADER) + " not implemented yet!")
            # The enable-signal on header 3 is set to 1 by default, no settings are required
            no_op
        case 4:
            req_mbus_en[4].set_value(LINE_MBUS_EN[4],Value.ACTIVE)

async def disable_motor_mult(CLICKBOARD_HEADER):
    #global req_mbus_en
    match CLICKBOARD_HEADER:
        case 1:
            #print("Disable for Clickboard-Header " + str(CLICKBOARD_HEADER) + " not implemented yet!")
            #Disable-signal not reacting, leave it as it is.
            no_op
        case 2:
            command_path= EN_PWM_PIN + "/enable"
            # Writing 0 disables the channel and sets a logic 0 on the pin
            subprocess.run("echo 0 > " + command_path, shell=True)
        case 3:
            #print("Disable for Clickboard-Header " + str(CLICKBOARD_HEADER) + " not implemented yet!")
            #Disable-signal not reacting, leave it as it is.
            no_op
        case 4:
            req_mbus_en[4].set_value(LINE_MBUS_EN[4],Value.INACTIVE)


async def activate_pwm(CLICKBOARD_HEADER):
    #activate the correct PWM for the appropriate socket:
    pwm_path=PWM_BEAGLE[CLICKBOARD_HEADER]
    if not os.path.exists(pwm_path):
        with open(pwm_path + "/export", "w") as export_file:
            export_file.write("0\n")



sleep_val = 0.1


async def main():
    #global CLICKBOARD_HEADER
    #global req_mbus_an
    #global req_mbus_rst
    #global req_mbus_en
    logging.basicConfig(level=logging.WARNING)
    _logger = logging.getLogger(__name__)
    #
    print("Started")
    ## define and control lines on Clickboard
    # Setup the server
    if len(sys.argv)>1:
        if sys.argv[1].isdigit():
            argv_header = sys.argv[1]
    else:
        argv_header = 4
        
    match(argv_header):
        case '1':
            CLICKBOARD_HEADER=1
            PORT = PORT_1
            req_mbus_an[1]  = gpiod.request_lines("/dev/gpiochip2",consumer="toggle_an_1",config={LINE_MBUS_AN[1]: gpiod.LineSettings(direction=Direction.OUTPUT, output_value=Value.ACTIVE)})
            req_mbus_rst[1] = gpiod.request_lines("/dev/gpiochip3",consumer="toggle_rst_1",config={LINE_MBUS_RST[1]: gpiod.LineSettings(direction=Direction.OUTPUT, output_value=Value.ACTIVE)})
            #req_mbus_en[1]  = gpiod.request_lines("/dev/gpiochip2",consumer="toggle_en_1",config={LINE_MBUS_EN[1]: gpiod.LineSettings(direction=Direction.OUTPUT, output_value=Value.ACTIVE)}) # SPI_1_SS1
        case '2':
            CLICKBOARD_HEADER=2
            PORT = PORT_2
            req_mbus_an[2]  = gpiod.request_lines("/dev/gpiochip2",consumer="toggle_an_2",config={LINE_MBUS_AN[2]: gpiod.LineSettings(direction=Direction.OUTPUT, output_value=Value.ACTIVE)})
            req_mbus_rst[2] = gpiod.request_lines("/dev/gpiochip3",consumer="toggle_rst_2",config={LINE_MBUS_RST[2]: gpiod.LineSettings(direction=Direction.OUTPUT, output_value=Value.ACTIVE)})
            #req_mbus_en[2]  = gpiod.request_lines("/dev/gpiochip2",consumer="toggle_en_2",config={LINE_MBUS_EN[2]: gpiod.LineSettings(direction=Direction.OUTPUT, output_value=Value.ACTIVE)}) # needs to be set via PWM on full on/off
        case '3':
            CLICKBOARD_HEADER=3
            PORT = PORT_3
            req_mbus_an[3]  = gpiod.request_lines("/dev/gpiochip2",consumer="toggle_an_3",config={LINE_MBUS_AN[3]: gpiod.LineSettings(direction=Direction.OUTPUT, output_value=Value.ACTIVE)})
            req_mbus_rst[3] = gpiod.request_lines("/dev/gpiochip2",consumer="toggle_rst_3",config={LINE_MBUS_RST[3]: gpiod.LineSettings(direction=Direction.OUTPUT, output_value=Value.ACTIVE)})
            #req_mbus_en[3]  = gpiod.request_lines("/dev/gpiochip2",consumer="toggle_en_3",config={LINE_MBUS_EN[3]: gpiod.LineSettings(direction=Direction.OUTPUT, output_value=Value.ACTIVE)}) # SPI_0_SS1
        case '4':
            CLICKBOARD_HEADER=4
            PORT = PORT_4
            req_mbus_an[4]  = gpiod.request_lines("/dev/gpiochip2",consumer="toggle_an_4",config={LINE_MBUS_AN[4]: gpiod.LineSettings(direction=Direction.OUTPUT, output_value=Value.ACTIVE)})
            req_mbus_rst[4] = gpiod.request_lines("/dev/gpiochip2",consumer="toggle_rst_4",config={LINE_MBUS_RST[4]: gpiod.LineSettings(direction=Direction.OUTPUT, output_value=Value.ACTIVE)})
            req_mbus_en[4]  = gpiod.request_lines("/dev/gpiochip2",consumer="toggle_en_4",config={LINE_MBUS_EN[4]: gpiod.LineSettings(direction=Direction.OUTPUT, output_value=Value.ACTIVE)})
        case _:
            CLICKBOARD_HEADER=4
            PORT = PORT_4
            req_mbus_an[4]  = gpiod.request_lines("/dev/gpiochip2",consumer="toggle_an_4",config={LINE_MBUS_AN[4]: gpiod.LineSettings(direction=Direction.OUTPUT, output_value=Value.ACTIVE)})
            req_mbus_rst[4] = gpiod.request_lines("/dev/gpiochip2",consumer="toggle_rst_4",config={LINE_MBUS_RST[4]: gpiod.LineSettings(direction=Direction.OUTPUT, output_value=Value.ACTIVE)})
            req_mbus_en[4]  = gpiod.request_lines("/dev/gpiochip2",consumer="toggle_en_4",config={LINE_MBUS_EN[4]: gpiod.LineSettings(direction=Direction.OUTPUT, output_value=Value.ACTIVE)})

    print("Using Clickboard header: " + str(CLICKBOARD_HEADER))
    print("GPIOD setup done, starting server. \nThis will take about a minute...")
    print("OPC/UA port: " + str(PORT))

    server = Server()
    await server.init()
    server.set_security_policy([ua.SecurityPolicyType.NoSecurity])
    url = "opc.tcp://0.0.0.0:" + PORT
    server.set_endpoint(url)
    print("Server setup done")

    name = "Stepper Motor Control - OPC Server"
    addspace = await server.register_namespace(name)

    node = server.get_objects_node()
    motor_control_object = await node.add_object(addspace, "Stepper Motor Controls")

    motor_start = await motor_control_object.add_variable(
        addspace, "Stepper Motor Start (1111 - Start)", 0
    )
    motor_stop = await motor_control_object.add_variable(
        addspace, "Stepper Motor Stop (1111 - Stop)", 0
    )
    motor_dir = await motor_control_object.add_variable(
        addspace, "Stepper Motor Direction (0-Clockwise, 1-Anti Clockwise)", 0
    )
    motor_enable = await motor_control_object.add_variable(
        addspace, "Stepper Motor Enable (1111 - Enable)", 0
    )
    motor_disable = await motor_control_object.add_variable(
        addspace, "Stepper Motor Disable (1111 - Disable)", 0
    )
    motor_reset = await motor_control_object.add_variable(
        addspace, "Stepper Motor Reset", 0
    )
    motor_steps = await motor_control_object.add_variable(
        addspace, "Stepper Motor Steps (200|400|800)", 200
    )
    sleep_time = await motor_control_object.add_variable(
        addspace, "Sleeptime between moves [ms]", 800
    )

    motor_speed = await motor_control_object.add_variable(
        addspace, "Stepper Motor Speed (1-Full, 2-Half, 3-Quarter, 0-Standby)", 3
    )
    command_control = await motor_control_object.add_variable(
        addspace, "Motor Command Control (1111 - Updates Speed/Direction)", 0000
    )
    print("command_control")
    await motor_start.set_writable()
    await motor_stop.set_writable()
    await motor_dir.set_writable()
    await motor_enable.set_writable()
    await motor_disable.set_writable()
    await motor_reset.set_writable()
    await motor_steps.set_writable()
    await sleep_time.set_writable()
    await motor_speed.set_writable()
    await command_control.set_writable()

    _logger.info("Starting server!")
    print("Stepper Motor - OPC-UA Server Started")

    # valid values
    valid_steps = [200, 400, 800]
    max_dir = 1
    max_speed = 3
    max_sleep = 3000

    # default values
    default_steps = 200
    default_dir = 0
    default_speed = 0
    default_sleep = 1000


    async with server:
        try:
            while True:
                start = await motor_start.get_value()
                stop = await motor_stop.get_value()
                dir = await motor_dir.get_value()
                enable = await motor_enable.get_value()
                disable = await motor_disable.get_value()
                reset = await motor_reset.get_value()
                steps = await motor_steps.get_value()
                speed = await motor_speed.get_value()
                t_sleep = await sleep_time.get_value()
                command_control_value = await command_control.get_value()

                # Check and correct steps value
                if steps not in valid_steps:
                    steps = default_steps
                    await motor_steps.set_value(default_steps)
                # Check and correct dir value
                if dir > max_dir:
                    dir = default_dir
                    await motor_dir.set_value(default_dir)
                # Check and correct speed value
                if speed > max_speed:
                    speed = default_speed
                    await motor_speed.set_value(default_speed)
                if t_sleep > max_sleep:
                    speed = default_sleep
                    await motor_speed.set_value(default_sleep)


                if command_control_value == 1111:
                    global sleep_val
                    # Configure the speed and the steps
                    if speed == 1:
                        print("Configuring the Stepper Motor Speed - Full")
                        subprocess.call(["i2cset", "-y", "0", "0x70", "0x1", "0xfd"])
                        # Dictionary to map/use steps to sleep_val
                        sleep_values = {200: 0.05, 400: 0.1, 800: 0.2}
                        sleep_val = sleep_values.get(steps)
                    elif speed == 2:
                        print("Configuring the Stepper Motor Speed - Half")
                        subprocess.call(["i2cset", "-y", "0", "0x70", "0x1", "0xfb"])
                        # Dictionary to map/use steps to sleep_val
                        sleep_values = {200: 0.1, 400: 0.2, 800: 0.4}
                        sleep_val = sleep_values.get(steps)
                    elif speed == 3:
                        print("Configuring the Stepper Motor Speed - Quarter")
                        subprocess.call(["i2cset", "-y", "0", "0x70", "0x1", "0xff"])
                        # Dictionary to map/use steps to sleep_val
                        sleep_values = {200: 0.2, 400: 0.4, 800: 0.8}
                        sleep_val = sleep_values.get(steps)
                    else:
                        print("Configuring the Stepper Motor Speed - StandBy")
                        subprocess.call(["i2cset", "-y", "0", "0x70", "0x1", "0xf9"])

                    if dir == 0:
                        print("Configuring the Stepper Motor Direction - Clockwise..., , Clickboard Header " + str(CLICKBOARD_HEADER))
                    else:
                        print("Configuring the Stepper Motor Direction - Anti Clockwise..., Clickboard Header " + str(CLICKBOARD_HEADER))


                    #direction = "20=" + str(dir)
                    # set the direction (0 - Clockwise, 1 - Anti Clockwise)
                    await set_direction_mult(LINE_MBUS_AN[CLICKBOARD_HEADER], dir, req_mbus_an[CLICKBOARD_HEADER])

                    await command_control.set_value(0000)

                if start == 1111:
                    print("Starting the Motor, Clickboard Header " + str(CLICKBOARD_HEADER))

                    await activate_pwm(CLICKBOARD_HEADER)

                    await reset_motor_mult(LINE_MBUS_RST[CLICKBOARD_HEADER], req_mbus_rst[CLICKBOARD_HEADER])

                    # set the GPIO expander's direction of I/O pins (port 1,2) as Output
                    subprocess.call(["i2cset", "-y", "0", "0x70", "0x3", "0xf9"])

                    await enable_motor_mult(CLICKBOARD_HEADER)#req_mbus_en)

                    # Set rate to 1 ms second (1000000 nanoseconds)
                    await set_pwm_rate_mult(PWM_BEAGLE[CLICKBOARD_HEADER], 1000000)

                    # Set duty cycle to 50% (500000 nanoseconds)
                    await set_pwm_duty_cycle_mult(PWM_BEAGLE[CLICKBOARD_HEADER], 500000)

                    command_path= PWM_BEAGLE[CLICKBOARD_HEADER] + "/enable"

                    # Enable the channel
                    subprocess.run("echo 1 > " + command_path, shell=True)
                    # Run the clock signal for configured steps
                    await asyncio.sleep(sleep_val)
                    # Disable the channel
                    subprocess.run("echo 0 > " + command_path, shell=True)
                    await disable_motor_mult(CLICKBOARD_HEADER)
                    # set the GPIO expander's direction of I/O pins (port 1,2) as Input

                    subprocess.call(["i2cset", "-y", "0", "0x70", "0x3", "0xff"])

                    #The following line will stop the motor after the one "bump"
                    #await motor_start.set_value(0)

                if stop == 1111:
                    print("Stopping the Motor, Clickboard Header " + str(CLICKBOARD_HEADER))
                    # Reset the motor
                    await reset_motor_mult(LINE_MBUS_RST[CLICKBOARD_HEADER], req_mbus_rst[CLICKBOARD_HEADER])

                    await disable_motor_mult(CLICKBOARD_HEADER)
                    await motor_start.set_value(0)
                    await motor_stop.set_value(0)

                if enable == 1111:
                    print("Enabling the Motor, Clickboard Header " + str(CLICKBOARD_HEADER))
                    await enable_motor_mult(CLICKBOARD_HEADER)
                    await motor_enable.set_value(0)

                if disable == 1111:
                    print("Disabling the Motor, Clickboard Header " + str(CLICKBOARD_HEADER))
                    await disable_motor_mult(CLICKBOARD_HEADER)

                    await motor_disable.set_value(0)

                if reset == 1111:
                    print("Resetting the Motor, Clickboard Header " + str(CLICKBOARD_HEADER))
                    # Reset the motor
                    await reset_motor_mult(LINE_MBUS_RST[CLICKBOARD_HEADER], req_mbus_rst[CLICKBOARD_HEADER])

                    await disable_motor_mult(CLICKBOARD_HEADER)
                    await motor_reset.set_value(0)
                await asyncio.sleep(t_sleep/1000)
        finally:
            # set the GPIO expander's direction of I/O pins (port 1,2) as Input
            subprocess.call(["i2cset", "-y", "0", "0x70", "0x3", "0xff"])
            # set the stepper motor speed to default i.e. Quarter Speed
            subprocess.call(["i2cset", "-y", "0", "0x70", "0x1", "0xff"])
            await disable_motor_mult(CLICKBOARD_HEADER)

            await server.stop()
            print("OPC Server stopped, PORT " + str(PORT))


if __name__ == "__main__":
    asyncio.run(main())
