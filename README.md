# BeagleV OPC/UA
This is a port of the motor control part of the OPC/UA application note for the PolarFire SoC Icicle Kit to the BeagleV Fire:

https://www.microchip.com/en-us/solutions/industrial/fpga/opc-ua

![Image of BeagleV with mikroBUS cape and Multi Click Stepper](./Images/one_stepper_sm.jpg)

The PDF of the original application note is located here:

https://ww1.microchip.com/downloads/aemDocuments/documents/FPGA/ApplicationNotes/ApplicationNotes/OPC_UA_Industrial_Edge_Demo.pdf

## Cape Design Requirements
This OPC/UA stack is meant to run with the default Cape design coming with the BeagleV board with the M.2 option included. 


## Hardware Requirements
The stepper motors are controlled via the mikroBUS Cape adapter-card in conjunction with one or more Multi Stepper Click boards:

https://www.mikroe.com/beaglebone-mikrobus-cape

https://www.mikroe.com/multi-stepper-click-tb67s102

As control interface the UAExpert software is used, accessible free of charge here:

www.unifiedautomation.com/fileadmin/files/client/uaexpert-bin-win32-x86-vs2008sp1-v1.6.3-448.zip

## Software Requirements on BeagleV
- GPIOD
```
sudo apt install python3-dev
pip3 install gpiod --break-system-packages
```
- Free OPC/UA (this step takes quite a long time, one hour+)
```
pip3 install asyncua --break-system-packages
pip3 install opcua --break-system-packages
```


## Usage
The usage of the Python-scripts slightly differs if one or multiple motors need to be controlled. 

### BeagleV, single motor
To run the OPC/UA stack one needs to start the python-script via:
```
python3 beagle_OPCUA_multi_click_0v3.py <header-socket>
```
Header-socket can range from 1..4, all four are functionally supported. If no header-socket is given then by default socket #4 is used.

Each socket listens to an individual port on the Ethernet-interface

- socket 1 = port 4841
- socket 2 = port 4842
- socket 3 = port 4843
- socket 4 = port 4844

### BeagleV, single motor
As multiple instances of the script need to be run, elevate your shell to root-level first and then start the Python-script in background mode:
```
sudo bash
python3 beagle_OPCUA_multi_click_0v3.py <header-socket> &
```
Note that different sockets need to be used, otherwise collisions will occur on accessing the GPIOs.

Each socket listens to an individual port on the Ethernet-interface

- socket 1 = port 4841
- socket 2 = port 4842
- socket 3 = port 4843
- socket 4 = port 4844

### UAExpert
Please refer to the [original OPC/UA application note](https://www.microchip.com/en-us/solutions/industrial/fpga/opc-ua) for usage of UAExpert.
To connect to the individual hosts running on the BeagleV board, set the appropriate port-numbers in the properties of every single connection:

![Screenshot of UAExpert to connect for different ports](./Images/UAExpert_Setup.PNG)
